package com.blubank.doctorappointment.controller.validation;






import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidDate {

    String message() default "Start date must before End Date.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}