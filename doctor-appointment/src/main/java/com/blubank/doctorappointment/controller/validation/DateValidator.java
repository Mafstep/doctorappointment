package com.blubank.doctorappointment.controller.validation;

import com.blubank.doctorappointment.controller.dto.CreateOpenAppointmentsRequest;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class DateValidator implements ConstraintValidator<ValidDate, CreateOpenAppointmentsRequest> {

    @Override
    public boolean isValid(CreateOpenAppointmentsRequest value, ConstraintValidatorContext context) {
        return !value.end().isBefore(value.start());
    }
}