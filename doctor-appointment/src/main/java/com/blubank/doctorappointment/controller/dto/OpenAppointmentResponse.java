package com.blubank.doctorappointment.controller.dto;

import com.blubank.doctorappointment.entity.Appointment;

import java.time.LocalDateTime;

public record OpenAppointmentResponse(
        Long id,
        LocalDateTime startTime,
        LocalDateTime endTime

) {
    public static OpenAppointmentResponse fromEntity(Appointment appointment) {
        return new OpenAppointmentResponse(appointment.getId(), appointment.getStartTime(), appointment.getEndTime());
    }
}
