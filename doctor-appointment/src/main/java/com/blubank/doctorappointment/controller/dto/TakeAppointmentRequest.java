package com.blubank.doctorappointment.controller.dto;

import jakarta.validation.constraints.NotNull;

public record TakeAppointmentRequest(
        @NotNull String name,
        @NotNull String phoneNumber
) {
}
