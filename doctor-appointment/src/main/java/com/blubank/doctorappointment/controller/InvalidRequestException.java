package com.blubank.doctorappointment.controller;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidRequestException extends RuntimeException {
    public InvalidRequestException(BindingResult result) {
        super(result.getAllErrors().get(0).getDefaultMessage());
    }
}
