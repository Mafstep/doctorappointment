package com.blubank.doctorappointment.controller;


import com.blubank.doctorappointment.controller.dto.OpenAppointmentResponse;
import com.blubank.doctorappointment.controller.dto.TakeAppointmentRequest;
import com.blubank.doctorappointment.entity.Appointment;
import com.blubank.doctorappointment.service.PatientAppointmentService;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/patient/appointment")
class PatientAppointmentController {

    private final PatientAppointmentService patientAppointmentService;

    public PatientAppointmentController(PatientAppointmentService patientAppointmentService) {
        this.patientAppointmentService = patientAppointmentService;
    }

    @GetMapping("open")
    Collection<OpenAppointmentResponse> getOpenAppointment(@RequestParam("date") String date) {
        return patientAppointmentService.getOpenAppointments(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE))
                .stream()
                .map(OpenAppointmentResponse::fromEntity)
                .collect(Collectors.toList());
    }

    @PostMapping("{id}")
    Appointment takeAppointment(@RequestBody TakeAppointmentRequest dto, @PathVariable Long id) {
        return patientAppointmentService.takeAppointment(id, dto.phoneNumber(), dto.name());
    }


    @GetMapping("/scheduled/{phoneNumber}")
    Collection<Appointment> getScheduledAppointment(@PathVariable String phoneNumber) {
        return patientAppointmentService.getTakenAppointmentByPhoneNumber(phoneNumber);
    }
}
