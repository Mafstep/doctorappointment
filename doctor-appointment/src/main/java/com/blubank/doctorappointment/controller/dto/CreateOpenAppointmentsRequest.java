package com.blubank.doctorappointment.controller.dto;

import com.blubank.doctorappointment.controller.validation.ValidDate;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;

import java.time.LocalDateTime;


@ValidDate
public record CreateOpenAppointmentsRequest(
        @NotNull
        @JsonFormat(pattern = "yyyy-MM-ddHH:mm:ss")
        LocalDateTime start,
        @NotNull
        @JsonFormat(pattern = "yyyy-MM-ddHH:mm:ss")
        LocalDateTime end) {


}
