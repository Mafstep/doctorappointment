package com.blubank.doctorappointment.controller;


import com.blubank.doctorappointment.controller.dto.CreateOpenAppointmentsRequest;
import com.blubank.doctorappointment.controller.dto.OpenAppointmentResponse;
import com.blubank.doctorappointment.entity.Appointment;
import com.blubank.doctorappointment.service.DoctorAppointmentService;
import jakarta.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/doctor/schedule")
class DoctorAppointmentController {


    private final DoctorAppointmentService appointmentService;

    DoctorAppointmentController(DoctorAppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @PostMapping
    Collection<OpenAppointmentResponse> createOpenAppointments(@Valid @RequestBody CreateOpenAppointmentsRequest dto, BindingResult result) {
        if (result.hasErrors()) throw new InvalidRequestException(result);
        return appointmentService.addAvailability(dto.start(), dto.end()).stream().map(OpenAppointmentResponse::fromEntity).collect(Collectors.toList());
    }

    @GetMapping
    Iterable<Appointment> getAppointments(@RequestParam("date") String date) {
        return appointmentService.getAppointments(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE));
    }

    @DeleteMapping("{id}")
    void deleteAppointment(@PathVariable Long id) {
        appointmentService.deleteAppointment(id);
    }


}
