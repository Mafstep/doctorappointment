package com.blubank.doctorappointment.service;

import com.blubank.doctorappointment.entity.Appointment;
import com.blubank.doctorappointment.repository.AppointmentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;

@Service
public class SimplePatientAppointmentService implements PatientAppointmentService {

    private final AppointmentRepository appointmentRepository;

    public SimplePatientAppointmentService(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }


    @Override
    public Collection<Appointment> getOpenAppointments(LocalDate date) {
        return appointmentRepository.findAllByStartTimeBetweenAndPatientNameIsNull(date.atStartOfDay(), date.plusDays(1).atStartOfDay());
    }

    @Transactional
    @Override
    public Appointment takeAppointment(Long id, String phoneNumber, String name) {
        var appointment = appointmentRepository.findById(id).orElseThrow(() -> new NotFoundException("Appointment Not Found"));
        if (appointment.getPatientName() != null) throw new AppointmentAlreadyScheduled();
        appointment.setPatientName(name);
        appointment.setPatientPhoneNumber(phoneNumber);
        return appointmentRepository.save(appointment);
    }

    @Override
    public Collection<Appointment> getTakenAppointmentByPhoneNumber(String phoneNumber) {
        return appointmentRepository.findAllByAndPatientPhoneNumber(phoneNumber);
    }
}
