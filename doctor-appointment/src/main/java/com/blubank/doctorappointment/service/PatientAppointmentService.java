package com.blubank.doctorappointment.service;

import com.blubank.doctorappointment.entity.Appointment;

import java.time.LocalDate;
import java.util.Collection;

public interface PatientAppointmentService {

    Collection<Appointment> getOpenAppointments(LocalDate date);

    Appointment takeAppointment(Long id, String phoneNumber, String name);

    Collection<Appointment> getTakenAppointmentByPhoneNumber(String phoneNumber);


}
