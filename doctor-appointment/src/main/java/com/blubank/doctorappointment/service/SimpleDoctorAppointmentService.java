package com.blubank.doctorappointment.service;

import com.blubank.doctorappointment.entity.Appointment;
import com.blubank.doctorappointment.repository.AppointmentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SimpleDoctorAppointmentService implements DoctorAppointmentService {

    private final AppointmentRepository appointmentRepository;

    public SimpleDoctorAppointmentService(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    @Override
    public Collection<Appointment> addAvailability(LocalDateTime start, LocalDateTime end) {
        Collection<Appointment> available = new ArrayList<>();
        LocalDateTime current = start;
        while (current.plusMinutes(30).isBefore(end)) {
            LocalDateTime appointmentEndTime = current.plusMinutes(30);
            available.add(new Appointment(current, appointmentEndTime));
            current = appointmentEndTime;
        }
        var appointments = appointmentRepository.saveAll(available);
        return StreamSupport
                .stream(appointments.spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Appointment> getAppointments(LocalDate date) {
        return appointmentRepository.findAllByStartTimeAfterAndEndTimeBefore(date.atStartOfDay(), date.plusDays(1).atStartOfDay());
    }

    @Transactional
    @Override
    public Appointment deleteAppointment(Long id) {
        var appointment = appointmentRepository.findById(id).orElseThrow(() -> new NotFoundException("Appointment Not Found"));
        if (appointment.getPatientName() != null) throw new AppointmentAlreadyScheduled();
        appointmentRepository.delete(appointment);
        return appointment;
    }

}
