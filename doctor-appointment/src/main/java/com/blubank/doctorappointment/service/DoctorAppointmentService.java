package com.blubank.doctorappointment.service;

import com.blubank.doctorappointment.entity.Appointment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

public interface DoctorAppointmentService {

    Collection<Appointment> addAvailability(LocalDateTime start, LocalDateTime end);

    Collection<Appointment> getAppointments(LocalDate date);

    Appointment deleteAppointment(Long id);


}
