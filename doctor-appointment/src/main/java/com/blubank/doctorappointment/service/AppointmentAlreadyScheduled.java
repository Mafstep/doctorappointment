package com.blubank.doctorappointment.service;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class AppointmentAlreadyScheduled extends RuntimeException {
    public AppointmentAlreadyScheduled() {
        super("This appointment is already scheduled by the patient");
    }
}
