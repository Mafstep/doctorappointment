package com.blubank.doctorappointment.repository;

import com.blubank.doctorappointment.entity.Appointment;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Repository
public interface AppointmentRepository extends CrudRepository<Appointment, Long> {

    Collection<Appointment> findAllByStartTimeAfterAndEndTimeBefore(LocalDateTime start, LocalDateTime end);

    Collection<Appointment> findAllByStartTimeBetweenAndPatientNameIsNull(LocalDateTime start, LocalDateTime end);

    Collection<Appointment> findAllByAndPatientPhoneNumber(String phoneNumber);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Appointment> findById(Long id);

}
