package com.blubank.doctorappointment.controller;

import com.blubank.doctorappointment.entity.Appointment;
import com.blubank.doctorappointment.service.AppointmentAlreadyScheduled;
import com.blubank.doctorappointment.service.PatientAppointmentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PatientAppointmentController.class)
class PatientAppointmentControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PatientAppointmentService patientAppointmentService;

    @Test
    void getOpenAppointment_whenEverythingIsOk_ThenItShouldReturnAllOpenAppointments() throws Exception {

        var appointment = new Appointment(LocalDateTime
                .of(2023, 3, 12, 8, 0, 0), LocalDateTime
                .of(2023, 3, 12, 8, 30, 0));
        when(patientAppointmentService.getOpenAppointments(any())).thenReturn(
                Collections.singleton(appointment));

        mockMvc.perform(get("/api/patient/appointment/open?date=2023-08-12"))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(status().isOk());
    }

    @Test
    void getOpenAppointment_whenWrongDateOrThereIsNoAppointment_itShouldReturnEmptyList() throws Exception {

        when(patientAppointmentService.getOpenAppointments(any())).thenReturn(
                Collections.emptyList());

        mockMvc.perform(get("/api/patient/appointment/open?date=2023-08-13"))
                .andExpect(jsonPath("$.length()").value(0))
                .andExpect(status().isOk());

    }

    @Test
    void takeAppointment_whenEverythingIsOk_itShouldScheduleAppointement() throws Exception {

        when(patientAppointmentService.takeAppointment(any(), any(), any())).thenReturn(new Appointment());

        mockMvc.perform(post("/api/patient/appointment/1")
                        .contentType("application/json")
                        .content("{\n" +
                                "\"name\":\"Ali\"," +
                                "\"phoneNumber\":\"09121111111\"" +
                                "}"))
                .andExpect(status().isOk());
    }

    @Test
    void takeAppointment_whenNameOrPhoneNumberIsNull_itShouldReturnBadRequest() throws Exception {


        mockMvc.perform(post("/api/patient/appointment/1")
                        .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void takeAppointment_whenTheAppointmentIsScheduledByPatient_itShouldReturn406() throws Exception {

        when(patientAppointmentService.takeAppointment(any(), any(), any()))
                .thenThrow(new AppointmentAlreadyScheduled());

        mockMvc.perform(post("/api/patient/appointment/1")
                        .contentType("application/json")
                        .content("{\n" +
                                "\"name\":\"Ali\"," +
                                "\"phoneNumber\":\"09121111111\"" +
                                "}"))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    void getScheduledAppointment_whenThereIsNoScheduledAppointment_itShouldReturnEmptyCollection() throws Exception {

        when(patientAppointmentService.getTakenAppointmentByPhoneNumber(any())).thenReturn(Collections.emptyList());


        mockMvc.perform(get("/api/patient/appointment/scheduled/09121111111"))
                .andExpect(jsonPath("$.length()").value(0))
                .andExpect(status().isOk());
    }

    @Test
    void getScheduledAppointment_whenNameOrPhoneNumberIsNull_itShouldReturnBadRequest() throws Exception {


        when(patientAppointmentService.getTakenAppointmentByPhoneNumber(any())).thenReturn(Arrays.asList(new Appointment(), new Appointment()));


        mockMvc.perform(get("/api/patient/appointment/scheduled/09121111111"))
                .andExpect(jsonPath("$.length()").value(2))

                .andExpect(status().isOk());
    }


}