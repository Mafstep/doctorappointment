package com.blubank.doctorappointment.controller;

import com.blubank.doctorappointment.entity.Appointment;
import com.blubank.doctorappointment.service.AppointmentAlreadyScheduled;
import com.blubank.doctorappointment.service.DoctorAppointmentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DoctorAppointmentController.class)
public class DoctorAppointmentControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private DoctorAppointmentService doctorAppointmentService;


    @Test
    void schedule_whenEverythingIsOk_itShouldReturnOpenAppointments() throws Exception {
        String requestBody = "{" +
                "\"start\":\"2023-03-1208:00:00\"," +
                "\"end\":\"2023-03-1208:30:00\"" +
                "}";
        when(doctorAppointmentService.addAvailability(any(), any())).thenReturn(
                Collections.singleton(
                        new Appointment(LocalDateTime
                                .of(2023, 3, 12, 8, 0, 0), LocalDateTime
                                .of(2023, 3, 12, 8, 30, 0))));
        mockMvc.perform(post("/api/doctor/schedule")
                        .contentType("application/json")
                        .content(requestBody))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].startTime").value("2023-03-12T08:00:00"))
                .andExpect(jsonPath("$[0].endTime").value("2023-03-12T08:30:00"));
    }

    @Test
    void schedule_whenWrongDateSent_itShouldReturn400() throws Exception {
        String requestBody = "{" +
                "\"start\":\"2023-03-1208:00:00\"," +
                "\"end\":\"2023-03-1207:30:00\"" +
                "}";

        mockMvc.perform(post("/api/doctor/schedule")
                        .contentType("application/json")
                        .content(requestBody))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getAppointments_whenEverythingIsOk_itShouldReturnOpenAppointments() throws Exception {
        var appointment1 = new Appointment(LocalDateTime
                .of(2023, 3, 12, 8, 0, 0), LocalDateTime
                .of(2023, 3, 12, 8, 30, 0));
        var appointment = new Appointment(LocalDateTime
                .of(2023, 3, 12, 8, 0, 0), LocalDateTime
                .of(2023, 3, 12, 8, 30, 0));
        appointment.setPatientPhoneNumber("09121111111");
        appointment.setPatientName("Ali Alizadeh");

        when(doctorAppointmentService.getAppointments(any())).thenReturn(
                Arrays.asList(appointment1, appointment));

        mockMvc.perform(get("/api/doctor/schedule?date=2023-08-12"))
                .andExpect(jsonPath("$.[1].patientPhoneNumber").value("09121111111"))
                .andExpect(jsonPath("$.[1].patientName").value("Ali Alizadeh"))
                .andExpect(status().isOk());
    }

    @Test
    void getAppointments_whenWrongDateOrThereIsNoAppointment_itShouldReturnEmptyList() throws Exception {

        when(doctorAppointmentService.getAppointments(any())).thenReturn(
                Collections.emptyList());

        mockMvc.perform(get("/api/doctor/schedule?date=2023-08-12"))
                .andExpect(jsonPath("$.length()").value(0))
                .andExpect(status().isOk());
    }

    @Test
    void deleteAppointment_whenEverythingIsOk_itShouldDeleteAppointement() throws Exception {

        when(doctorAppointmentService.deleteAppointment(any())).thenReturn(any());

        mockMvc.perform(delete("/api/doctor/schedule/1"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteAppointment_whenTheAppointmentIsScheduledByPatient_itShouldReturn406() throws Exception {

        when(doctorAppointmentService.deleteAppointment(any())).thenThrow(new AppointmentAlreadyScheduled());

        mockMvc.perform(delete("/api/doctor/schedule/2"))
                .andExpect(status().isNotAcceptable());
    }
}